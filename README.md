# README #

This is an example Unity (version 5.3.4f1) project I set up to do a tutorial on writing shaders. The slides that go along with this can be found [here](https://docs.google.com/presentation/d/1-Sw1vMMBVVU_JEwNG6AL01U40RhaYXNEp1kfhO3Hbvc/edit?usp=sharing).

The 3D models used in this project were downloaded from the Unity Asset Store [here](https://www.assetstore.unity3d.com/en/#!/content/58355).