﻿Shader "Custom/SolidColorShader"
{
	Properties
	{
		_Color ("Main Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vertexShader
			#pragma fragment fragmentShader
			
			struct vertexInput
			{
				float4 vertex : POSITION;
			};

			struct vertexOutput
			{
				float4 vertex : SV_POSITION;
			};

			float4 _Color;
			
			vertexOutput vertexShader(vertexInput input)
			{
				vertexOutput output;
				output.vertex = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			fixed4 fragmentShader() : SV_Target
			{
				return _Color;
			}
			ENDCG
		}
	}
}
