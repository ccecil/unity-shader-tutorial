﻿Shader "Custom/TextureColorChangeShader"
{
	Properties
	{
		_Texture("Main Texture", 2D) = "white" {}
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vertexShader
			#pragma fragment fragmentShader
			
			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			sampler2D _Texture;
			
			vertexOutput vertexShader(vertexInput input)
			{
				vertexOutput output;
				output.vertex = mul(UNITY_MATRIX_MVP, input.vertex);
				output.uv = input.uv;
				return output;
			}
			
			fixed4 fragmentShader(vertexOutput input) : SV_Target
			{
				float4 color = tex2D(_Texture, input.uv);
				float blueValue = color.b;
				color.b = color.r;
				color.r = blueValue;
				return color;
			}
			ENDCG
		}
	}
}
