﻿Shader "Custom/GrayscaleImageEffectShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexShader
			#pragma fragment fragmentShader
			
			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct vertexOutput
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			vertexOutput vertexShader(vertexInput input)
			{
				vertexOutput output;
				output.vertex = mul(UNITY_MATRIX_MVP, input.vertex);
				output.uv = input.uv;
				return output;
			}
			
			sampler2D _MainTex;

			fixed4 fragmentShader(vertexOutput input) : SV_Target
			{
				fixed4 color = tex2D(_MainTex, input.uv);
				return Luminance(color);
			}
			ENDCG
		}
	}
}
