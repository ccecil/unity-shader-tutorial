﻿Shader "Custom/WavyShader"
{
	Properties
	{
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vertexShader
			#pragma fragment fragmentShader

			#include "UnityCG.cginc"
			
			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 objectNormal : NORMAL;
			};

			struct vertexOutput
			{
				float4 vertex : SV_POSITION;
				float3 worldNormal : TEXCOORD0;
			};

			vertexOutput vertexShader(vertexInput input)
			{
				vertexOutput output;

				float4 worldVertex = mul(_Object2World, input.vertex);
				worldVertex.z = worldVertex.z + sin(worldVertex.x * 100 + _Time[3] * 2) * .01;
				output.vertex = mul(UNITY_MATRIX_VP, worldVertex);

				output.worldNormal = UnityObjectToWorldNormal(input.objectNormal);

				return output;
			}
			
			fixed4 fragmentShader(vertexOutput input) : SV_Target
			{
				float4 color = 0;
				color.rgb = input.worldNormal * .5 + .5;
				return color;
			}
			ENDCG
		}
	}
}
